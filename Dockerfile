FROM python:3.8-alpine
LABEL maintainer="mark@londonappdeveloper.com"

RUN pip install awscli==1.17.13

ENTRYPOINT ["aws"]
